#!/usr/bin/env python3
# -*- coding: utf8 -*-

import sys
import codecs
import tempfile
import subprocess
from jinja2 import Template
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QFile, QIODevice, QXmlStreamReader
from PyQt5.QtWidgets import (QWidget, QLabel, QGroupBox, QTreeWidgetItem,
                             QGridLayout, QVBoxLayout, QTreeWidget, QToolBar,
                             QFileDialog, QAction, QMainWindow, QApplication)


class Iface(QWidget):

    def __init__(self):
        QWidget.__init__(self)
        self.grid = QGridLayout(self)

        # EUI-EUP Info
        self.eui_group = QGroupBox('EUI-EUP Info')
        self.eui_part_number_label = QLabel('<b>Part Number:</b>')
        self.eui_serial_number_label = QLabel('<b>Serial Number:</b>')
        self.eui_original_code_label = QLabel('<b>Original Code:</b>')
        self.eui_new_code_label = QLabel('<b>New Code:</b>')
        self.eui_overal_test_pass_label = QLabel(
            '<b>Overall Test Pass/Fail:</b>')

        # Adding widget to box.
        self.eui_box = QVBoxLayout(self.eui_group)
        self.eui_box.addWidget(self.eui_part_number_label)
        self.eui_box.addWidget(self.eui_serial_number_label)
        self.eui_box.addWidget(self.eui_original_code_label)
        self.eui_box.addWidget(self.eui_new_code_label)
        self.eui_box.addWidget(self.eui_overal_test_pass_label)

        # DateTime & Operator
        self.info_group = QGroupBox('When and Who made measure')
        self.info_test_time_label = QLabel('<b>Test Time:</b>')
        self.info_operator_label = QLabel('<b>Operator:</b>')
        self.info_owner_label = QLabel('<b>Owner:</b>')

        # Adding widget to box.
        self.info_box = QVBoxLayout(self.info_group)
        self.info_box.addWidget(self.info_test_time_label)
        self.info_box.addWidget(self.info_operator_label)
        self.info_box.addWidget(self.info_owner_label)

        self.result_table = QTreeWidget()
        self.result_table.headerItem().setText(0, u'No.')
        self.result_table.headerItem().setText(1, u'Result Information')
        self.result_table.headerItem().setText(2, u'Value')
        self.result_table.headerItem().setText(3, u'Min')
        self.result_table.headerItem().setText(4, u'Max')
        self.result_table.headerItem().setText(5, u'Units')
        self.result_table.headerItem().setText(6, u'Status')
        self.result_table.setColumnWidth(1, 250)
        self.result_table.setAlternatingRowColors(True)

        self.adjustment_table = QTreeWidget()
        self.adjustment_table.headerItem().setText(0, u'Drive Speed (RPM)')
        self.adjustment_table.headerItem().setText(1,
                                                   u'Supply Temperature (°C)')
        self.adjustment_table.headerItem().setText(
            2, u'Injector Temperature (°C)')
        self.adjustment_table.headerItem().setText(3, u'Supply Pressure (bar)')
        self.adjustment_table.headerItem().setText(4, u'Base Load (Volts)')
        self.adjustment_table.setAlternatingRowColors(True)
        for i in range(5):
            self.adjustment_table.setColumnWidth(i, 185)

        self.result_label = QLabel('Result Details:')

        self.grid.addWidget(self.eui_group, 0, 0)
        self.grid.addWidget(self.info_group, 0, 1)
        self.grid.addWidget(self.result_label, 1, 0)
        self.grid.addWidget(self.result_table, 2, 0)
        self.grid.addWidget(self.adjustment_table, 2, 1)

        self.box_dict = {
            "@PE_OwnerName": (self.info_owner_label,
                              '<b>Owner:</b>', 'owner'),
            "@PE_Operator": (self.info_operator_label,
                             '<b>Operator:</b>', 'operator'),
            "@PE_TestDateTime": (self.info_test_time_label,
                                 '<b>Test Time:</b>', 'test_time'),
            "@PE_InjRef": (self.eui_part_number_label,
                           '<b>Part Number:</b>', 'part_number'),
            "@PE_Injector_S/N": (self.eui_serial_number_label,
                                 '<b>Serial Number:</b>', 'serial_number'),
            "@PE_OldCode": (self.eui_original_code_label,
                            '<b>Original Code:</b>', 'original_code'),
        }

        self.number = 1
        self.result_list = list()

    def open(self):
        self.number = 1
        self.result_list = list()
        filename = QFileDialog.getOpenFileName(self, 'Open file', '.',
                                               "DRF files (*.drf);;"
                                               "XML files (*.xml);;")
        fn = filename[0].split('/')[-1].split('.')[0]

        if not filename:
            return
        self.result_label.setText('Result Details: %s' % fn)
        self.result_table.clear()
        self.adjustment_table.clear()
        self.pass_test = True
        self.html_dict = dict()
        file = QFile(filename[0])
        file.open(QIODevice.ReadOnly)
        self.xml = QXmlStreamReader(file)
        while not self.xml.atEnd():
            if self.xml.isStartElement() and self.xml.name() == "Module":
                attr = self.xml.attributes()
                if int(attr.value('NumberOfCommands')):
                    if attr.value('ModuleName') == "@PE_ResultDetails":
                        self.parse_result_details()
                    elif attr.value('ModuleName') == "@PE_ElectricalResults":
                        self.parse_electrical_results()
                    elif self.xml.attributes().value('ModuleName') == "@PE_DiagnosticResults":
                        self.parse_diagnostic_results()
                    elif attr.value('ModuleName') == "@PE_AdjustmentResults":
                        self.parse_adjustment_results()
            self.xml.readNext()

        file.close()
        self.make_html()

        if self.pass_test:
            self.eui_overal_test_pass_label.setText(
                '<b>Overall Test Pass/Fail: '
                '<font color="green">Pass</font></b>')
        else:
            self.eui_overal_test_pass_label.setText(
                '<b>Overall Test Pass/Fail: '
                '<font color="red">Fail</font></b>')

    def parse_result_details(self):
        while not self.xml.atEnd():
            name = self.xml.attributes().value("Name")
            value = self.xml.attributes().value("Value")
            if name in self.box_dict.keys():
                pair = self.box_dict.get(str(name))
                pair[0].setText("%s %s" % (pair[1], value))
                self.html_dict[pair[2]] = str(value)
            self.xml.readNext()
            if self.xml.name() == 'Module':
                break

    def parse_electrical_results(self):

        result = dict()
        item = QTreeWidgetItem(self.result_table)
        item.setText(0, str(self.number))
        item.setText(1, 'Electrical Results')
        result['number'] = self.number
        result['name'] = 'Electrical Results'
        self.number += 1
        self.result_list.append(result)
        result = dict()
        result['number'] = self.number
        item = QTreeWidgetItem(self.result_table)
        item.setText(0, str(self.number))
        self.number += 1
        item.setText(1, 'SCV Resistance')
        result['name'] = 'SCV Resistance'
        while not self.xml.atEnd():
            name = str(self.xml.attributes().value("Name"))
            value = str(self.xml.attributes().value("Value"))
            units = str(self.xml.attributes().value("Units"))
            if name == '@PE_SCVResistance':
                item.setText(2, value)
                result['value'] = value
                item.setText(5, units)
                result['units'] = units
            elif name == '@PE_MinLimit':
                item.setText(3, value)
                result['minimum'] = value
            elif name == '@PE_MaxLimit':
                item.setText(4, value)
                result['maximum'] = value
            elif name == '@PE_StepPassFail':
                if value == '1':
                    item.setText(6, 'OK')
                    result['status'] = 'OK'
                self.pass_test &= (value == '1')
                self.result_list.append(result)
            elif name == '@PE_ValveTestOK':
                result = dict()
                result['number'] = self.number
                item = QTreeWidgetItem(self.result_table)
                item.setText(0, str(self.number))
                self.number += 1
                item.setText(1, 'Valve Test')
                result['name'] = 'Valve Test'
                valve_test = 'OK' if value == '1' else 'Fail'
                item.setText(6, valve_test)
                result['status'] = valve_test
                self.result_list.append(result)
            self.xml.readNext()
            if self.xml.name() == 'Module':
                break

    def parse_diagnostic_results(self):
        result = dict()
        item = QTreeWidgetItem(self.result_table)
        item.setText(0, str(self.number))
        item.setText(1, 'Diagnostic Results')
        result['number'] = self.number
        result['name'] = 'Diagnostic Results'
        self.number += 1
        self.result_list.append(result)
        result = dict()
        result['number'] = self.number
        while not self.xml.atEnd():
            self.xml.readNext()
            value = str(self.xml.attributes().value("Value"))
            name = str(self.xml.attributes().value("Name"))
            minimum = str(self.xml.attributes().value("Min"))
            maximum = str(self.xml.attributes().value("Max"))
            units = str(self.xml.attributes().value("Units"))
            try:
                status = 'OK' if float(minimum) <= float(value) <= float(maximum) else 'Fail'
            except ValueError:
                status = ''

            if self.xml.name() == 'Module':
                break
            if not value:
                continue
            elif value and minimum and minimum != maximum:
                result = dict()
                item = QTreeWidgetItem(self.result_table)
                item.setText(0, str(self.number))
                result['number'] = self.number
                self.number += 1
                named = self.__get_name(name)
                item.setText(1, named)
                result['name'] = named
                item.setText(2, value)
                result['value'] = value
                item.setText(3, minimum)
                result['minimum'] = minimum
                item.setText(4, maximum)
                result['maximum'] = maximum
                item.setText(5, units)
                result['units'] = units
                item.setText(6, status)
                result['status'] = status
                self.pass_test &= (value == '1')
                self.result_list.append(result)
            else:
                continue
        self.html_dict['result'] = self.result_list

    def parse_adjustment_results(self):
        item = QTreeWidgetItem(self.result_table)
        item.setText(0, str(self.number))
        result = dict()
        result['number'] = self.number
        self.number += 1
        adjustiment_data = {
            "@PE_DriveSpeed": 0,
            "@PE_SupplyTemp": 1,
            "@PE_InjectorTemp": 2,
            "@PE_SupplyPress": 3,
            "@PE_BaseLoad": 4
        }
        item.setText(1, 'Adjustment Results')
        result['name'] = 'Adjustment Results'
        self.result_list.append(result)
        self.result_table.addTopLevelItem(item)
        adj_item = QTreeWidgetItem(self.adjustment_table)
        while True:
            self.xml.readNext()
            name = str(self.xml.attributes().value("Name"))
            value = str(self.xml.attributes().value("Value"))
            if self.xml.name() == 'Module':
                break
            if not value:
                continue
            minimum = str(self.xml.attributes().value("Min"))
            maximum = str(self.xml.attributes().value("Max"))
            units = str(self.xml.attributes().value("Units"))

            try:
                status = 'OK' if float(minimum) <= float(
                    value) <= float(maximum) else 'Fail'
            except ValueError:
                status = ''

            if name == '@PE_EUIEUPCode':
                self.eui_new_code_label.setText('<b>New Code:</b> %s' % value)
                self.html_dict['new_code'] = value

            elif name == '@PE_StepPassFail':
                self.pass_test &= (value == '1')

            elif name in adjustiment_data.keys():
                if (name == "@PE_DriveSpeed" and
                        any([adj_item.text(i) for i in range(1, 5)])):
                    adj_item = QTreeWidgetItem(self.adjustment_table)
                    adj_item.setText(0, value)
                else:
                    adj_item.setText(adjustiment_data.get(name), value)

            elif value and minimum and minimum != maximum:
                result = dict()
                item = QTreeWidgetItem(self.result_table)
                item.setText(0, str(self.number))
                result['number'] = self.number
                self.number += 1
                named = self.__get_name(name)
                item.setText(1, named)
                result['name'] = named
                item.setText(2, value)
                result['value'] = value
                item.setText(3, minimum)
                result['minimum'] = minimum
                item.setText(4, maximum)
                result['maximum'] = maximum
                item.setText(5, units)
                result['units'] = units
                item.setText(6, status)
                result['status'] = status
                self.result_list.append(result)
            else:
                continue
        self.html_dict['result'] = self.result_list

    @staticmethod
    def __get_name(name):
        named = ''
        try:
            n = name.split('_')[1]
        except IndexError:
            n = name
        for i, s in enumerate(n):
            try:
                b = ord(n[i+1])
            except IndexError:
                b = 0
            if ord(s) >= 90 and b <= 90:
                named += '%s ' % s
            else:
                named += s
        named = named.lstrip()
        if named[:3] in ['SCV', 'NCV']:
            named = '%s %s' % (named[:3], named[3:])
        return named

    def make_html(self):
        with open('template.html') as tmplt:
            html = tmplt.read()
            template = Template(html)
            self.html = template.render(self.html_dict)

    def make_pdf(self):

        filename = QFileDialog.getSaveFileName(self, 'Save as PDF', '',
                                               'PDF File (*.pdf)')

        if not filename:
            return
        else:
            filename = filename[0]
        self.make_html()
        if not filename.endswith('.pdf'):
            filename = u'%s%s' % (filename, '.pdf')
        tmp = tempfile.mkstemp('.html')[1]
        with codecs.open(tmp, 'wb', 'utf-8') as tmp_desc:
            tmp_desc.write(self.html)
        command = u"wkhtmltopdf {} {}".format(tmp, filename)
        subprocess.check_call(command, shell=True)


class MainWindow(QMainWindow):

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setWindowTitle(u'Reading Diagnostic Information')
        self.setWindowIcon(QIcon('icon/iris.ico'))
        self.showMaximized()

        self.central = Iface()
        self.setCentralWidget(self.central)
        self.statusBar()

        self.toolbar = QToolBar()
        self.addToolBar(self.toolbar)

        self.menubar = self.menuBar()
        self.file = self.menubar.addMenu(u'&File')

        # Open
        self.open = QAction(QIcon('icon/open.png'), u'&Open', self)
        self.open.setShortcut('Ctrl+O')
        self.file.addAction(self.open)
        self.open.triggered.connect(self.central.open)
        self.toolbar.addAction(self.open)

        # Save as PDF
        self.pdf = QAction(QIcon('icon/pdf.png'), u'Import to PDF', self)
        self.pdf.setShortcut('Ctrl+S')
        self.file.addAction(self.pdf)
        self.pdf.triggered.connect(self.central.make_pdf)
        self.toolbar.addAction(self.pdf)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    iface = MainWindow()
    iface.show()
    sys.exit(app.exec_())
