
Reading Diagnostic Information
==============================

The application for reading DRF-files, which showing information about diagnostic of car, and also it have a possibility make report in PDF. 



For Windows if you see a need for build binary file:
----------------------------------------------------

Need installing:

* Python version 3.5

* PyQt5 for python 3.5

* wkhtmltopdf


And using tool *pip* need installing:

* jinja2

* cx_Freeze 

For example:

::

    pip install cx_freeze   
    pip install jinja2


And then, perform it command:

::

    python setup.py bdist_msi


Possibly are you need will make second build, previously put to build directory several files and catalogs, including:

* **template.html**

* **icon/**

* **wkthmltopdf** or **wkhtmltopdf.exe**, it depense from platform.

* **wkhtmltox.so** or **wkhtmltox.dll**

* **mscvt100.dll**, **mscvt120.dll** - if wkhtmltopdf was build with using Visual Studio, or **libgcc_s_dw2-1.dll** if the build was made with using MinGW.


Screenshot
----------


.. image:: doc/screen.png


*Screenshot from Mac OS X*
