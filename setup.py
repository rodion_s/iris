from cx_Freeze import setup, Executable


shortcut_table = [
    ("DesktopShortcut",        # Shortcut
     "DesktopFolder",          # Directory_
     "IRIS",                   # Name
     "TARGETDIR",              # Component_
     "[TARGETDIR]iris.exe",    # Target
     None,                     # Arguments
     None,                     # Description
     None,                     # Hotkey
     None,                     # Icon
     None,                     # IconIndex
     None,                     # ShowCmd
     'TARGETDIR'               # WkDir
     )
]

msi_data = {"Shortcut": shortcut_table}
bdist_msi_options = {'data': msi_data}

setup(
    options={
        "bdist_msi": bdist_msi_options,
    },
    name="IrisReader",
    version="1.0",
    author="http://profserver.ru",
    author_email="service@profserver.ru",
    url="http://profserver.ru",
    description="The Reader information about testing cars",
    executables=[Executable("iris.pyw", base="Win32GUI", icon="iris.ico")],
)
